<!DOCTYPE html>
<html>
<head>
    <title>Developer Test</title>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css" integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">
    <link rel="stylesheet" href="style/style.css">
</head>
<body class="back-color">
<?php
/**
 * Variable my name
 */
$myName = "Maksim";

/**
 * Binery code from task
 */
$binary = "01110000 01110010 01101001 01101110 01110100 00100000 01101111 01110101 01110100 
        00100000 01111001 01101111 01110101 01110010 00100000 01101110 01100001 01101101 01100101 00100000 
        01110111 01101001 01110100 01101000 00100000 01101111 01101110 01100101 00100000 01101111 01100110 
        00100000 01110000 01101000 01110000 00100000 01101100 01101111 01101111 01110000 01110011";

/**
 * @param $binary
 * @return string|null
 * Function that converts string - binary code in to string
 */
function binaryToString($binary)
{
    $binaries = explode(' ', $binary);

    $string = null;
    foreach ($binaries as $binary) {
        $string .= pack('H*', dechex(bindec($binary)));
    }

    return $string;
}

/**
 * @param $number
 * @param $myName
 * Function return argument name several times - depends from param $number
 */
function loopMyName($number, $myName) {
    if ($number >= 25) {
        echo "You dont`t love me so much";
    } elseif ($number <= 0) {
        echo "Don`t be so cold :)";
    } else {
        while ($number > 0) {
            echo "$myName"."<br>";
            $number--;
        }
    }
}
?>
<div class="container">
    <div><h1 class="text-center mt-5">TASK 1 - Name</h1></div>
    <div class="mt-1">
        <p class="text-center">Binary code: </p>
        <p><?php echo "$binary";?></p>
        <p class="text-center">means: </p>
        <h5 class="text-center"><?php echo binaryToString($binary);?></h5>
    </div>
    <hr>
    <div class="row">
        <div class="col-9">
            <form action="index.php" method="post">
                <p style="display: inline">How many times you want to see my name ?</p>
                <input type="number" name="loopNumber" id="loopNumber">
                <input type="submit" name="loopMyName" value="Push me">
            </form>
        </div>
        <div class="col-3">
            <p>
                <?php
                /**
                 * Server logic for form on UI
                 */
                if ($_SERVER['REQUEST_METHOD'] == "POST" and isset($_POST['loopMyName'])) {
                    loopMyName($_POST['loopNumber'], $myName);
                }
                ?></p>
        </div>
    </div>
</div>
</body>
</html>